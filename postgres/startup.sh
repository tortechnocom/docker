#!/bin/bash
if [ -z "$(runuser -l postgres -c 'psql -l | grep ofbiz')" ]
then
    runuser -l postgres -c "createdb ofbiz"
    runuser -l postgres -c "createdb ofbizolap"
    runuser -l postgres -c "createuser root -sw"
    runuser -l postgres -c "psql -l"
    psql -d postgres -c "create user ${PSQL_USER} with password '${PSQL_PASSWORD}' superuser;"
fi
